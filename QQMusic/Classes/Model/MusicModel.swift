//
//  MusicModel.swift
//  QQMusic
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class MusicModel: NSObject {
    /// 歌曲名称
    var name : String = ""
    /// MP3文件的名称
    var filename : String = ""
    /// 歌词文件的名称
    var lrcname : String = ""
    /// 歌手的名称
    var singer : String = ""
    /// 封面的图片名称
    var icon : String = ""
    
    
    init(dict : [String : Any]) {
        super.init()
        
        setValuesForKeys(dict)
    }
    override func setValue(_ value: Any?, forUndefinedKey key: String) {}
}
